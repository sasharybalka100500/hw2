const mongoose = require('mongoose');

const noteSchema = new mongoose.Schema({
  userId: String,
  completed: Boolean,
  text: String,
  createdDate: Date,
});

module.exports = mongoose.model('Note', noteSchema);
