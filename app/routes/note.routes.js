const Note = require('../models/note.model.js');
const auth = require('../../configs/auth');
const mongoose = require('mongoose');
module.exports = (app) => {
  app.post('/api/notes', auth, async (req, res) => {
    if (!req.body.text) {
      return res.status(400).send({
        message: 'Note text can not be empty',
      });
    }

    const note = new Note({
      userId: req.user.user_id,
      completed: false,
      text: req.body.text,
      createdDate: Date.now(),
    });

    note
        .save()
        .then(() => {
          console.log('Success');
          res.status(200).send({
            message: 'Success',
          });
        })
        .catch((err) => {
          res.status(500).send({
            message:
              err.message || 'Some error occurred while creating the Note.',
          });
        });
  });

  app.get('/api/notes', auth, async (req, res) => {
    const count = await Note.find({userId: req.user.user_id});
    Note.find({userId: req.user.user_id})
        .skip(req.query.offset)
        .limit(req.query.limit)
        .then((notes) => {
          console.log(notes);
          res.status(200).send({
            offset: Number(req.query.offset || 0),
            limit: Number(req.query.limit || 0),
            count: count.length,
            notes: notes,
          });
        })
        .catch((err) => {
          console.log('Some error occurred while retrieving notes.');
          res.status(500).send({
            message: 'Some error occurred while retrieving notes.',
          });
        });
  });

  app.get('/api/notes/:noteId', auth, async (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.params.noteId)) {
      await Note.findById(req.params.noteId).then((targetNote) => {
        if (targetNote.userId !== req.user.user_id) {
          console.log(
              `Current user don't have note with id ${req.params.noteId}`,
          );
          res.status(400).send({
            message: `Curent user don't have note with id ${req.params.noteId}`,
          });
        } else {
          console.log(targetNote);
          res.status(200).send({
            note: targetNote,
          });
        }
      });
    } else {
      res.status(400).send({
        message: `Note with id  ${req.params.noteId} doesn't exist`,
      });
    }
  });

  app.put('/api/notes/:noteId', auth, async (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.params.noteId)) {
      await Note.findById(req.params.noteId).then((targetNote) => {
        if (targetNote.userId !== req.user.user_id) {
          console.log(
              `Current user don't have note with id ${req.params.noteId}`,
          );
          res.status(400).send({
            message: `Curent user don't have note with id ${req.params.noteId}`,
          });
        } else {
          if (!req.body.text) {
            console.log(`Text is required`);
            res.status(400).send({
              message: `Text is required`,
            });
          } else {
            targetNote.text = req.body.text;
            targetNote.save();
            console.log(`Success`);
            res.status(200).send({
              message: 'Success',
            });
          }
        }
      });
    } else {
      console.log(`Note with id  ${req.params.noteId} doesn't exist`);
      res.status(400).send({
        message: `Note with id  ${req.params.noteId} doesn't exist`,
      });
    }
  });

  app.patch('/api/notes/:noteId', auth, async (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.params.noteId)) {
      await Note.findById(req.params.noteId).then((targetNote) => {
        if (targetNote.userId !== req.user.user_id) {
          console.log(
              `Current user don't have note with id ${req.params.noteId}`,
          );
          res.status(400).send({
            message: `Curent user don't have note with id ${req.params.noteId}`,
          });
        } else {
          targetNote.completed = !targetNote.completed;
          targetNote.save();
          console.log(`Success`);
          res.status(200).send({
            message: 'Success',
          });
        }
      });
    } else {
      console.log(`Note with id  ${req.params.noteId} doesn't exist`);
      res.status(400).send({
        message: `Note with id  ${req.params.noteId} doesn't exist`,
      });
    }
  });

  app.delete('/api/notes/:noteId', auth, async (req, res) => {
    if (mongoose.Types.ObjectId.isValid(req.params.noteId)) {
      await Note.findById(req.params.noteId).then((targetNote) => {
        if (targetNote.userId !== req.user.user_id) {
          console.log(
              `Current user don't have note with id ${req.params.noteId}`,
          );
          res.status(400).send({
            message: `Curent user don't have note with id ${req.params.noteId}`,
          });
        } else {
          targetNote.remove();
          console.log('Success');
          res.status(200).send({
            message: 'Success',
          });
        }
      });
    } else {
      console.log(`Note with id  ${req.params.noteId} doesn't exist`);
      res.status(400).send({
        message: `Note with id  ${req.params.noteId} doesn't exist`,
      });
    }
  });
};
