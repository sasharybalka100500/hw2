module.exports = (app) => {
  const User = require('../models/user.model.js');
  const bcrypt = require('bcryptjs');
  const jwt = require('jsonwebtoken');
  const auth = require('../../configs/auth');
  app.post('/api/auth/register', async (req, res) => {
    try {
      const {username, password} = req.body;

      if (!(username && password)) {
        console.log('All input is required');
        return res.status(400).send({message: 'All input is required'});
      }

      const oldUser = await User.findOne({username});

      if (oldUser) {
        console.log('User Already Exist. Please Login');
        return res
            .status(400)
            .send({message: 'User Already Exist. Please Login'});
      }

      encryptedPassword = await bcrypt.hash(password, 10);

      const user = await User.create({
        username,
        password: encryptedPassword,
      });

      const token = jwt.sign(
          {
            user_id: user._id,
            username,
            createdDate: user.createdAt,
            password: user.password,
          },
          process.env.TOKEN_KEY,
          {
            expiresIn: '24h',
          },
      );
      user.token = token;
      console.log('Success');
      res.status(200).send({message: 'Success'});
    } catch (err) {
      console.log('Server error');
      res.status(500).send({message: 'Server error'});
    }
  });
  app.post('/api/auth/login', async (req, res) => {
    try {
      const {username, password} = req.body;

      if (!(username && password)) {
        console.log('All inputs is required');
        return res.status(400).send({message: 'All inputs is required'});
      }
      const user = await User.findOne({username});

      if (user && (await bcrypt.compare(password, user.password))) {
        const token = jwt.sign(
            {
              user_id: user._id,
              username,
              createdDate: user.createdAt,
              password: user.password,
            },
            process.env.TOKEN_KEY,
            {
              expiresIn: '24h',
            },
        );

        user.token = token;
        console.log('Success');
        return res
            .status(200)
            .send({message: 'Success', jwt_token: user.token});
      }
      console.log('Invalid Credentials');
      return res.status(400).send({message: 'Invalid Credentials'});
    } catch (err) {
      console.log('Server error');
      res.status(500).send({message: 'Server error'});
    }
  });
  app.get('/api/users/me', auth, async (req, res) => {
    try {
      const user = await User.findById(req.user.user_id).select('-password');
      console.log(user);
      res.status(200).send({
        user: {
          _id: user._id,
          username: user.username,
          createdDate: user.createdAt,
        },
      });
    } catch (err) {
      res.status(500).send({message: `Server error`});
      console.log(err);
    }
  });
  app.delete('/api/users/me', auth, async (req, res) => {
    try {
      User.findByIdAndRemove(req.user.user_id, (err, docs) => {
        if (err) {
          res.status(400).send({message: `Something wrong`});
          console.log('Something wrong');
        } else {
          res.status(200).send({message: `Success`});
          console.log('Success');
        }
      });
    } catch (err) {
      res.status(500).send({message: `Server error`});
      console.log(err);
    }
  });
  app.patch('/api/users/me', auth, async (req, res) => {
    try {
      if (!(req.body.oldPassword && req.body.newPassword)) {
        console.log(`Invalid Credentials`);
        return res.status(400).send({message: `Invalid Credentials`});
      }
      if (await bcrypt.compare(req.body.oldPassword, req.user.password)) {
        const pass = await bcrypt.hash(req.body.newPassword, 10);
        User.findByIdAndUpdate(
            {
              _id: req.user.user_id,
            },
            {
              $set: {password: pass},
            },
        ).then(() => {
          res.status(200).send({message: `Success`});
          console.log(`Success`);
        });
      } else {
        res.status(400).send({message: `Invalid Password`});
        console.log(`Invalid Password`);
      }
    } catch (err) {
      res.status(500).send({message: `Server error`});
      console.log(err);
    }
  });
};
